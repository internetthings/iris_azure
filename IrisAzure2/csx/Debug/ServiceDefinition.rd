﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="IrisAzure2" generation="1" functional="0" release="0" Id="46031092-6ef8-48c6-856f-baf7a7970fd0" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="IrisAzure2Group" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="IrisWebAPI:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/IrisAzure2/IrisAzure2Group/LB:IrisWebAPI:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="IrisWebAPI:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/IrisAzure2/IrisAzure2Group/MapIrisWebAPI:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="IrisWebAPIInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/IrisAzure2/IrisAzure2Group/MapIrisWebAPIInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:IrisWebAPI:Endpoint1">
          <toPorts>
            <inPortMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPI/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapIrisWebAPI:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPI/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapIrisWebAPIInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPIInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="IrisWebAPI" generation="1" functional="0" release="0" software="C:\Users\george\Documents\Visual Studio 2013\Projects\IrisAzure2\IrisAzure2\csx\Debug\roles\IrisWebAPI" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;IrisWebAPI&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;IrisWebAPI&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPIInstances" />
            <sCSPolicyUpdateDomainMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPIUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPIFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="IrisWebAPIUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="IrisWebAPIFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="IrisWebAPIInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="0292c7f9-fd38-40c8-bb65-f7c1b605813e" ref="Microsoft.RedDog.Contract\ServiceContract\IrisAzure2Contract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="311c4306-0469-4edd-a68c-bb55e4c94937" ref="Microsoft.RedDog.Contract\Interface\IrisWebAPI:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/IrisAzure2/IrisAzure2Group/IrisWebAPI:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>